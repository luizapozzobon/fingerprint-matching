import numpy as np
import cv2 as cv
import matplotlib.pylab as plt;
import copy

import csv

import enhance_image
import feature_extraction
import feature_match
import orientation_map

def main():
    #compare_all_to(110, False)
    compare_two('101_1', '101_1', True)

def compare_all_to(default_set = 101, show=False):

    n = 101
    dimgs = []
    for i in range(1, 9):
        name = str(default_set) + '_' + str(i)
        dimgs.append(name)

    results = []
    maxscores = []
    scores = []
    comparing = []
    points = []
    export = [comparing, results, maxscores, scores, points]
    for k in range(n, 111):
        for i in range(0,8): # i = numero d
            for j in range(1,9):
                img2 = str(k) + '_' + str(j)
                print('----------- Comparing ', dimgs[i], 'to', img2)
                result, maxscore, score = compare_two(dimgs[i], img2, show)
                if(k == default_set): expected = True
                else: expected = False
                
                if(result == expected): point = 1.0
                else: point = 0.0
                points.append(point)
                comparing.append((dimgs[i], img2))
                results.append(result)
                maxscores.append(maxscore)
                scores.append(score)
    
    for i in range(len(results)):
        print(comparing[i], results[i], scores[i], maxscores[i], points[i])
    print(export)
    print('Average rate of right matches = ', sum(points)/len(points)*100)
    
    with open('results'+ str(default_set) +'.csv', "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(export)

def compare_two(img1 = '101_1', img2 = '101_2', show=False):
    img1 = cv.imread('./DB/' + img1 + '.tif', 0)
    img2 = cv.imread('./DB/' + img2 + '.tif' , 0)
    skel1 = process(img1, show)
    skel2 = process(img2, show)
    kp1, des1 = feature_extraction.feature_extraction(skel1, show)
    kp2, des2 = feature_extraction.feature_extraction(skel2, show)
    matches = feature_match.feature_match(skel1, skel2, des1, des2, kp1, kp2, show)
    result, maxscore, score = feature_match.score(matches, threshold=70)
    return result, maxscore, score #result true is match

def process(img, show):
    img = resize(img)
    enhanced = enhance_image.enhance_image(img, show)
    return enhanced

def resize(img):
    rows, cols = img.shape
    aspect_ratio = np.float(rows)/np.float(cols)

    new_rows = 350
    new_cols = new_rows/aspect_ratio
    new_img = cv.resize(img, (np.int(new_rows), np.int(new_cols)))
    return new_img


if __name__ == "__main__":
    main()