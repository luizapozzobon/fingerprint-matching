import cv2 as cv
import numpy as np
import copy

from matplotlib import pyplot as plt
from skimage import io

def feature_match(img1, img2, des1, des2, kp1, kp2, show):
    # create BFMatcher object
    bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)
    # Match descriptors.
    matches = bf.match(des1,des2)
    # Sort them in the order of their distance.
    matches = sorted(matches, key = lambda x:x.distance)
    # Draw first 10 matches.
    img3 = cv.drawMatches(img1,kp1,img2,kp2,matches, None, flags=2)
    if show:
        plt.figure()
        io.imshow(img3)
        io.show()
    return matches

def score(matches, threshold):
    score = 0
    for m in matches:
        score += m.distance
    meanscore = score/len(matches)
    if(meanscore < threshold):
        print("Fingerprints Match")
        result = True
    else:
        print("Fingerprints do not match")
        result = False
    #print("Total Score: ", score)
    print("Threshold Score: ", meanscore)

    return result, score, meanscore
