# opencv

import cv2
import numpy as np

def build_filters():
    """ returns a list of kernels in several orientations
    """
    filters = []
    ksize = 31
    for theta in np.arange(0, np.pi, np.pi / 64):
        """ params = {'ksize':(ksize, ksize), 'sigma':1.0, 'theta':theta, 'lambd':15.0,
                  'gamma':0.02, 'psi':0, 'ktype':cv2.CV_32F} """
        params = {'ksize':(ksize, ksize), 'sigma': 3.5, 'theta':theta, 'lambd':9,
        'gamma': 1, 'psi':0, 'ktype':cv2.CV_32F}
        kern = cv2.getGaborKernel(**params)
        kern /= 2*kern.sum()
        filters.append((kern,params))
    return filters

def process(img, filters):
    """ returns the img filtered by the filter list
    """
    accum = np.zeros_like(img)
    accum2 = np.zeros_like(img)
    laststd = lastmean = 0
    for kern, params in filters:
        fimg = cv2.filter2D(img, cv2.CV_8UC3, kern)
        std = np.std(fimg)
        mean = np.mean(fimg)
        print(std, mean)
        if std > laststd: # std > laststd and 
            accum2 = fimg
            laststd = std
            lastmean = mean
        np.maximum(accum, fimg, accum)
    return accum, accum2

