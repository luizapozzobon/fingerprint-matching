import cv2 as cv
import numpy as np
import copy

from matplotlib import pyplot as plt
from skimage import io

def feature_extraction(skel_img, show):
    keypoints = harris_detection(skel_img, show)
    kp, des = keypoint_descriptors(skel_img, keypoints)
    return keypoints, des

def harris_detection(img, show):
    imgcopy = copy.copy(img)
    dst = cv.cornerHarris(img, 2, 3, 0.1)

    #result is dilated for marking the corners, not important
    dst = cv.dilate(dst,None)
    # Threshold for an optimal value, it may vary depending on the image.
    imgcopy[dst>0.15*dst.max()]=[150]
    #turn dst into keypoints
    keypoints = np.argwhere(dst > 0.15 * dst.max())
    keypoints = [cv.KeyPoint(x[1], x[0], 1) for x in keypoints]
    if show:
        plt.figure()
        io.imshow(imgcopy)
        io.show()
    return keypoints

def keypoint_descriptors(skel_img, keypoints):
    #sift = cv.xfeatures2d.SIFT_create()
    #kp, descriptors = sift.compute(img, keypoints)
    orb = cv.ORB_create()
    kp, descriptors = orb.compute(skel_img, keypoints)
    return kp, descriptors
