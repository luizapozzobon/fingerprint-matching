import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from skimage import io
from scipy import ndimage;

def orientation_map(img):
    gradientsigma = 1;
    blocksigma = 7;
    orientsmoothsigma = 7;

    rows, cols = img.shape

    sze = np.fix(6*gradientsigma);
    if np.remainder(sze,2) == 0:
        sze = sze+1;

    gauss = cv.getGaussianKernel(np.int(sze),gradientsigma);
    f = gauss * gauss.T
    
    fy,fx = np.gradient(f) 

    Gx = cv.Sobel(img,cv.CV_64F,1,0,ksize=3)
    Gy = cv.Sobel(img,cv.CV_64F,0,1,ksize=3)
    Gxx = np.power(Gx,2)
    Gyy = np.power(Gy,2)
    Gxy = Gx*Gy

    sze= np.fix(6*blocksigma)

    gauss = cv.getGaussianKernel(np.int(sze), blocksigma)
    f = gauss * gauss.T

    denom = np.sqrt(np.power(Gxy, 2) + np.power((Gxx - Gyy), 2) + np.finfo(float).eps)

    sin2theta = Gxy/denom
    cos2theta = (Gxx - Gyy)/denom

    if orientsmoothsigma:
        sze = np.fix(6*orientsmoothsigma)
        if np.remainder(sze,2)== 0:
            sze = sze+1
        gauss = cv.getGaussianKernel(np.int(sze), orientsmoothsigma)
        f = gauss * gauss.T
        cos2theta = ndimage.convolve(cos2theta, f)
        sin2theta = ndimage.convolve(sin2theta, f)

    orientim = np.pi/2 + np.arctan(sin2theta, cos2theta)/2
    plt.figure()
    io.imshow(orientim)
    io.show()