import cv2 as cv
import numpy as np

import gabor

from matplotlib import pyplot as plt
from skimage import exposure, io
from skimage.morphology import skeletonize

from skimage.feature import hog

def enhance_image(img, show):
    norm = normalize(img)
    divisions = 12  #best one was with 12
    filtered = gabor_filter(norm, divisions, show)
    binarized = binarize(filtered, show)
    skeleton = get_skeleton(binarized, show)
    return skeleton

def normalize(img):
    normed = np.zeros((img.shape[0], img.shape[1]))
    normed = cv.normalize(img,  normed, 0, 255, cv.NORM_MINMAX)
    return normed

def gabor_filter(nimg, divisions, show):
    gaborized = gabor.filter_image(nimg, divisions = divisions)
    #gaborized = normalize(gaborized)
    #gaborized = 2.0*gaborized
    if show:
        plt.figure()
        io.imshow(gaborized)
        io.show()
    return gaborized

def binarize(gabor_img, show):
    rows, cols = gabor_img.shape
    binarized = np.zeros((rows, cols))
    for i in range(0, rows):
        for j in range(0, cols):
            if gabor_img[i, j] == 0:
                binarized[i,j] = 1
            """ if gabor_img[i, j] == 255:
                binarized[i, j] = 1
            elif gabor_img[i, j] < 60:
                binarized[i, j] = 0 """
    kernel = np.ones((1,1),np.uint8)
    binarized = cv.erode(binarized,kernel,iterations = 1)
    kernel = np.ones((2,2),np.uint8)
    binarized = cv.dilate(binarized,kernel,iterations = 1)
    kernel = np.ones((2,2),np.uint8)
    binarized = cv.erode(binarized,kernel,iterations = 1)
    if show:
        plt.figure()
        io.imshow(binarized)
        io.show()
    return binarized

def get_skeleton(bimg, show):
    skel = skeletonize(bimg)
    if show:
        plt.figure()
        io.imshow(skel)
        io.show()
    skel = skel.astype(dtype='uint8')
    mask = skel > 0
    skel[mask] = 255
    return skel


# unused functions

def invert(img):
    inverted = np.invert(img)
    plt.figure()
    io.imshow(inverted)
    io.show()
    return inverted

def ridge_segment(img, blocksize, threshold):
    rows, cols = img.shape

    new_rows = np.int(blocksize * np.ceil( np.float(rows) / np.float(blocksize)))
    new_cols = np.int(blocksize * np.ceil( np.float(cols) / np.float(blocksize)))
    # numero adequado de colunas e linhas para fechar com o tamanho de blocos escolhidos
    # precisa encher de 0 pra completar o tamanho certo (maior que o inicial)
    
    padded_img = np.zeros((new_rows, new_cols))
    stddevimg = np.zeros((new_rows, new_cols))

    padded_img[0:rows][:, 0:cols] = copy.deepcopy(img)

    for i in range(0, new_rows, blocksize):
        for j in range(0, new_cols, blocksize):
            block = padded_img[i:i+blocksize][:, j:j+blocksize]
            stddevimg[i:i+blocksize][:, j:j+blocksize] = np.std(block) * np.ones(block.shape)

    
    stddevimg = stddevimg[0:rows][:, 0:cols]
    mask = stddevimg > threshold
    mean_val = np.mean(img[mask])
    std_val = np.std(img[mask])
    normimg = (img - mean_val)/std_val

    return(normimg, mask)
    
def ridge_orient(nimg):
    fd, hog_image = hog(nimg, orientations=16, pixels_per_cell=(16, 16),
                    cells_per_block=(1, 1), visualize=True)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)
    
    ax1.axis('off')
    ax1.imshow(nimg, cmap=plt.cm.gray)
    ax1.set_title('Input image')

    # Rescale histogram for better display
    hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 10))
    ax2.axis('off')
    ax2.imshow(hog_image, cmap=plt.cm.gray)
    ax2.set_title('Histogram of Oriented Gradients')
    plt.show()
    return hog_image