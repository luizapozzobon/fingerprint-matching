# opencv

import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage import io

def build_filters(divisions):
    """ returns a list of kernels in several orientations
    """
    filters = []

    ksize = 31
    """ if divisions >= 12:
        ksize = 21 """

    for theta in np.arange(0, np.pi, np.pi / 32):
        """ params = {'ksize':(ksize, ksize), 'sigma':1.0, 'theta':theta, 'lambd':15.0,
                  'gamma':0.02, 'psi':0, 'ktype':cv2.CV_32F} """
        params = {'ksize':(ksize, ksize), 'sigma': 3.5, 'theta':theta, 'lambd':8.5,
        'gamma': 1.0, 'psi':0, 'ktype':cv2.CV_32F}
        kern = cv2.getGaborKernel(**params)
        kern /= 1.5*kern.sum()
        filters.append((kern,params))
    return filters

def filter_image(img, divisions):
    """ returns the img filtered by the filter list
    """
    filters = build_filters(divisions)

    rows,cols = img.shape
    new_rows = np.int(divisions * np.ceil((np.float(rows)/(np.float(divisions)))))
    new_cols = np.int(divisions * np.ceil((np.float(cols)/(np.float(divisions)))))

    padded_img = np.ones((new_rows,new_cols))*255;
    padded_img[0:rows][:,0:cols] = img;

    blocksize1 = new_rows/divisions
    blocksize2 = new_cols/divisions

    accum = np.zeros((new_rows, new_cols))

    for i in range(0, new_rows, blocksize1):
        for j in range(0, new_cols, blocksize2):
            accum2 = np.zeros((blocksize1, blocksize2))
            laststd = lastmean = 0
            std = np.std(padded_img[i:i+blocksize1][:,j:j+blocksize2])
            mean = np.mean(padded_img[i:i+blocksize1][:,j:j+blocksize2])
            if std > 1:
                for kern, params in filters:
                    fimg = cv2.filter2D(padded_img[i:i+blocksize1][:,j:j+blocksize2], cv2.CV_8UC3, kern)
                    std = np.std(fimg)
                    mean = np.mean(fimg)
                    if std > laststd:
                        accum2 = fimg
                        laststd = std
                        lastmean = mean
                accum[i:i+blocksize1][:,j:j+blocksize2] = accum2
            else:
                accum[i:i+blocksize1][:, j:j+blocksize2] = 170
    return accum

#main
""" img = cv2.imread('../../DB/101_1.tif',0)
filters = build_filters()
p = process(img, filters)
print(p)
cv2.imshow('asd', p)
cv2.waitKey()
cv2.destroyAllWindows() """